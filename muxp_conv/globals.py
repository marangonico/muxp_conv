MUST_COMMAND_PARAMETERS = {
    "cut_polygon": ["coordinates"],
    "cut_ramp": ["coordinates", "3d_coordinates"],
    "cut_flat_terrain_in_mesh": ["coordinates", "terrain", "elevation"],
    "cut_spline_segment": ["3d_coordinates", "terrain", "width", "profile_interval"],
    "update_network_levels": ["coordinates", "road_coords_drapped"],
    "limit_edges": ["coordinates", "edge_limit"],
    "update_raster_elevation": ["coordinates", "elevation"],
    "update_raster4spline_segment": ["3d_coordinates", "width"],
    "update_elevation_in_poly": ["coordinates", "elevation"],
    "extract_mesh_to_file": ["coordinates"],
    "insert_mesh_from_file": ["coordinates", "terrain"],
    "exit_without_update": [],
}

MUXP_COMMANDS = (
    "cut_polygon",
    "cut_ramp",
    "cut_flat_terrain_in_mesh",
    "cut_spline_segment",
    "update_network_levels",
    "limit_edges",
    "update_raster_elevation",
    "update_raster4spline_segment",
    "update_elevation_in_poly",
    "extract_mesh_to_file",
    "insert_mesh_from_file",
    "exit_without_update",
)

MUXP_PARAMS = (
    'name',
    'elevation',
    'terrain',
    "include_raster_square_criteria",
    "edge_limit",
    "width",
    "profile_interval",
)

