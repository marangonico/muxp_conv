from collections import OrderedDict

from xmltodict import parse, unparse


class WED(object):

    wed_file = None
    wed_xml = None
    o = None
    world = None
    group_muxp = None
    max_id = 0

    ref_lat = 0
    ref_lon = 0

    def __init__(self, wed_file=None):
        self.wed_file = wed_file
        if self.wed_file:
            self.load()

    def load(self, wed_file=None):
        if wed_file:
            self.wed_file = wed_file
        self.wed_xml = parse(open(self.wed_file).read())
        self.o = self.wed_xml['doc']['objects']['object']
        self.world = self.get_object(class_name='Group', object_name='world')

        for o in self.o:
            if int(o['@id']) > self.max_id:
                self.max_id = int(o['@id'])

    @staticmethod
    def normalize_class_name(class_name):
        if class_name[0:3] != 'WED_':
            class_name = 'WED_' + class_name
        return class_name

    def get_object(self, class_name='', object_name='', object_id=''):

        class_name = self.normalize_class_name(class_name)

        try:
            object_id = str(object_id)
        except TypeError:
            pass

        for o in self.o:
            if object_name:
                if o['@class'] == class_name and o['hierarchy']['@name'].upper() == object_name.upper():
                    return o
            elif object_id:
                if o['@id'] == object_id:
                    return o

        return None

    def get_children(self, parent):

        children_objects = []

        for o in self.o:
            if o['@parent_id'] == parent['@id']:
                children_objects.append(o)

        return children_objects

    def add_object(self, class_name='', object_name='', parent=None, point=None):

        class_name = self.normalize_class_name(class_name)

        if self.get_object(class_name, object_name):
            raise Exception(f'object (class_name={class_name}, object_name={object_name} already exists')

        if parent is None:
            parent = self.group_muxp

        self.max_id += 1
        o = {
            '@class': class_name,
            '@id': str(self.max_id),
            '@parent_id': parent['@id'],
            'children': {},
            'hierarchy': {
                '@name': object_name, '@locked': '0', '@hidden': '0'
            },
        }

        if point:
            o['point'] = point

        self.o.append(o)

        if parent:
            if 'child' not in parent['children']:
                parent['children']['child'] = []

            if isinstance(parent['children']['child'], OrderedDict):
                child = parent['children']['child']
                parent['children']['child'] = [child, ]

            parent['children']['child'].append({'@id': o['@id']})

        return o

    def delete_children(self, children_ids=[]):

        for o in self.o:
            if o['@id'] in children_ids:
                if o['children']:
                    self.delete_children(o['children']['child'])
                self.o.remove(o)

    def delete_object(self, obj):

        for o in self.o:
            if o['@id'] == obj['@parent_id']:
                o['children']['child'].remove({'@id': obj['@id']})

        children_ids = [
            obj['@id'],
        ]

        self.delete_children(children_ids)

        if o['children']:
            children = o['children']['child']
            for child in children:
                self.delete_object(child)

        self.delete_children()

    def add_simple_bezier_boundary_node(self, object_name='', parent=None, lat=0, lon=0):
        point = {
            '@latitude': f'{lat}',
            '@longitude': f'{lon}',
            '@split': '0',
            '@ctrl_latitude_lo': '0.0',
            '@ctrl_longitude_lo': '0.0',
            '@ctrl_latitude_hi': '0.0',
            '@ctrl_longitude_hi': '0.0'
        }
        return self.add_object(
            class_name="SimpleBezierBoundaryNode", object_name=object_name, parent=parent, point=point)

    def add_group(self, object_name='', parent=None):
        return self.add_object('Group', object_name=object_name, parent=parent)

    def add_group_muxp(self):
        self.group_muxp = self.add_group('muxp', self.world)

    def add_poly(self, object_name='', parent=None, coords=[]):
        if parent is None:
            parent = self.group_muxp
        polygon = self.add_object('PolygonPlacement', object_name=object_name, parent=parent)
        ring = self.add_object('Ring', 'muxp_ring', polygon)
        for coord in coords:

            if len(coord) == 2:
                self.add_simple_bezier_boundary_node(
                    object_name='muxp_node', parent=ring, lat=coord[0], lon=coord[1])
            elif len(coord) == 3:
                self.add_simple_bezier_boundary_node(
                    object_name=str(coord[2]), parent=ring, lat=coord[0], lon=coord[1])

    def add_string(self, object_name='', parent=None, coords=[]):
        if parent is None:
            parent = self.group_muxp
        string = self.add_object('StringPlacement', object_name=object_name, parent=parent)
        for coord in coords:

            if len(coord) == 2:
                self.add_simple_bezier_boundary_node(
                    object_name='muxp_node', parent=string, lat=coord[0], lon=coord[1])
            elif len(coord) == 3:
                self.add_simple_bezier_boundary_node(
                    object_name=str(coord[2]), parent=string, lat=coord[0], lon=coord[1])

    def add_meta(self, object_name='', parent=None):
        point = {
            "@latitude": str(self.ref_lat),
            "@longitude": str(self.ref_lon),
            "@heading": 0,
        }
        return self.add_object('ObjPlacement', object_name=object_name, parent=parent, point=point)

    def get_meta(self, object_name='', parent=None):
        point = {
            "@latitude": str(self.ref_lat),
            "@longitude": str(self.ref_lon),
            "@heading": 0,
        }
        return self.get_object(class_name='ObjPlacement', object_name=object_name, parent=parent, point=point)

    def set_reference_values(self, parent=None, lat=0, lon=0):
        self.ref_lat = lat
        self.ref_lon = lon

    def emit_xml(self):
        return unparse(self.wed_xml, pretty=True)
