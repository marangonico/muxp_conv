import yaml

from muxp_conv.globals import MUST_COMMAND_PARAMETERS, MUXP_COMMANDS, MUXP_PARAMS
from muxp_conv.wed import WED


class MUXP(object):

    muxp_file = None
    muxp_yaml = None
    wed = None
    # wed_xml = None
    # o = None
    # world = None
    # max_id = 0

    def __init__(self, muxp_file=None):
        self.muxp_file = muxp_file
        if self.muxp_file:
            self.load()

    def load(self, muxp_file=None):
        if muxp_file:
            self.muxp_file = muxp_file

        with open(self.muxp_file) as file:
            self.muxp_yaml = yaml.load(file, Loader=yaml.FullLoader)

    @staticmethod
    def wed_check_and_add_meta(wed, parent, value, name):
        if name in value:
            wed.add_meta(object_name=f"{name}:{value[name]}", parent=parent)

    def wed_inject(self, wed_file=None):

        wed = WED(wed_file=wed_file)

        group_muxp = wed.get_object('Group', 'muxp')
        if group_muxp:
            wed.delete_object(group_muxp)

        wed.add_group_muxp()

        for k, v in self.muxp_yaml.items():

            commands = k.split('.')

            if commands[0] in ('muxp_version', 'id', 'version', 'description', 'author', 'tile', ):
                wed.add_meta(object_name=f"{k}:{v}")

            elif commands[0] == 'area':
                lats_lons = v.split()
                # v='45.392391  45.403086  6.625944  6.644899' -> ['45.392391', '45.403086', '6.625944', '6.644899']
                coords = [
                    (lats_lons[0], lats_lons[2]),
                    (lats_lons[1], lats_lons[2]),
                    (lats_lons[1], lats_lons[3]),
                    (lats_lons[0], lats_lons[3]),
                    (lats_lons[0], lats_lons[2]),
                ]
                wed.add_poly(
                    object_name='area:', coords=coords
                )

            else:

                if commands[0] in MUXP_COMMANDS:

                    g = wed.add_group(k)

                    if 'coordinates' in v:
                        ref_lat, ref_lon = v['coordinates'][0].split()
                    elif '3d_coordinates' in v:
                        ref_lat, ref_lon, elevation = v['3d_coordinates'][0].split()
                    elif 'road_coords_drapped' in v:
                        ref_lat, ref_lon, elevation = v['road_coords_drapped'][0].split()
                    else:
                        ref_lat = None

                    if ref_lat:
                        wed.set_reference_values(lat=ref_lat, lon=ref_lon)

                    for param in MUXP_PARAMS:
                        self.wed_check_and_add_meta(wed, g, v, param)

                    if 'coordinates' in v:
                        coords = []
                        for coord in v['coordinates'][0:-1]:
                            coords.append(coord.split())
                        wed.add_poly(object_name='coordinates', parent=g, coords=coords)

                    if '3d_coordinates' in v:
                        coords = []
                        for coord in v['3d_coordinates']:
                            coords.append(coord.split())
                        wed.add_string(object_name='3d_coordinates', parent=g, coords=coords)

                    if 'road_coords_drapped' in v:
                        coords = []
                        for coord in v['road_coords_drapped'][0:-1]:
                            coords.append(coord.split())
                        wed.add_poly(object_name='road_coords_drapped', parent=g, coords=coords)

        return wed.emit_xml()

    def wed_collect_area(self, wed_object):

        lat_min = 90
        lat_max = -90
        lon_min = 180
        lon_max = -180

        ring = self.wed.get_object(object_id=wed_object['children']['child']['@id'])
        for child in ring['children']['child']:
            o = self.wed.get_object(object_id=child['@id'])

            lat = float(o['point']['@latitude'])
            lon = float(o['point']['@longitude'])

            if lat > lat_max:
                lat_max = lat

            if lat < lat_min:
                lat_min = lat

            if lon > lon_max:
                lon_max = lon
            if lon < lon_min:
                lon_min = lon

        area = f"{lat_min}  {lat_max}  {lon_min}  {lon_max}"

        return area

    def wed_collect_coordinates(self, wed_object):

        coords = []

        ring = self.wed.get_object(object_id=wed_object['children']['child']['@id'])
        for child in ring['children']['child']:
            o = self.wed.get_object(object_id=child['@id'])
            lat = float(o['point']['@latitude'])
            lon = float(o['point']['@longitude'])
            # coords.append(f'{lat:18} {lon:18}')
            coords.append(f'{lat} {lon}')

        coords.append(coords[0])

        return coords

    def wed_collect_3d_coordinates(self, wed_object):

        coords = []

        if isinstance(wed_object['children']['child'], list):
            wed_coordinates = wed_object['children']['child']
        else:
            ring = self.wed.get_object(object_id=wed_object['children']['child']['@id'])
            wed_coordinates = ring['children']['child']

        for child in wed_coordinates:
            o = self.wed.get_object(object_id=child['@id'])
            lat = float(o['point']['@latitude'])
            lon = float(o['point']['@longitude'])

            object_name = o['hierarchy']['@name']
            try:
                if float(object_name) == int(float(object_name)):
                    elevation = int(float(object_name))
                else:
                    elevation = float(object_name)
            except ValueError as e:
                print(f"3d_coordinates: object name should be elevation, but is {object_name}")

            coords.append(f'{lat} {lon} {elevation}')

        if wed_object['@class'] != 'WED_StringPlacement':
            coords.append(coords[0])

        return coords

    def wed_collect_command(self, wed_object):

        cmd_yaml = {}

        if isinstance(wed_object['children']['child'], list):
            children = wed_object['children']['child']
        else:
            children = [wed_object['children']['child'], ]

        for child in children:

            o = self.wed.get_object(object_id=child['@id'])
            wed_command = o['hierarchy']['@name']

            if ':' in wed_command and o['@class'] == 'WED_ObjPlacement':
                key, value = wed_command.split(':')
                if key in ('muxp_version', 'version', 'elevation', 'width', 'profile_interval', ):
                    if float(value) == int(float(value)):
                        cmd_yaml[key] = int(float(value))
                    else:
                        cmd_yaml[key] = float(value)
                else:
                    cmd_yaml[key] = value

            elif wed_command == 'coordinates':
                cmd_yaml['coordinates'] = self.wed_collect_coordinates(o)

            elif wed_command == '3d_coordinates':
                cmd_yaml['3d_coordinates'] = self.wed_collect_3d_coordinates(o)

            pass

        return cmd_yaml

    def wed2muxp(self, wed_file=None):

        self.wed = WED(wed_file=wed_file)
        self.muxp_yaml = {}

        g = self.wed.get_object('Group', 'muxp')

        for child in g['children']['child']:
            o = self.wed.get_object(object_id=child['@id'])

            wed_command = o['hierarchy']['@name']

            if ':' in wed_command and o['@class'] == 'WED_ObjPlacement':
                key, value = wed_command.split(':')

                if key in ('muxp_version', 'version', ):
                    if float(value) == int(float(value)):
                        self.muxp_yaml[key] = int(float(value))
                    else:
                        self.muxp_yaml[key] = float(value)
                else:
                    self.muxp_yaml[key] = value

            elif ':' in wed_command and wed_command == 'area:':
                self.muxp_yaml['area'] = self.wed_collect_area(o)

            elif o['@class'] == 'WED_Group':

                # if '.' in wed_command:
                #     cmd, label = wed_command.split('.')
                # else:
                #     cmd = wed_command
                if o['hierarchy']['@hidden'] == '0':
                    self.muxp_yaml[wed_command] = self.wed_collect_command(o)

        # for command in ('muxp_version', 'id', 'version', 'description', 'author', 'tile', ):
        #     o = g.get_object()

        y = yaml.dump(
            self.muxp_yaml, default_flow_style=False, sort_keys=False).replace("\'", "")

        return y

