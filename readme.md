**Intro**

muxp_conv is a python script that supports the great [MUXP](https://forums.x-plane.org/index.php?/forums/topic/208622-announcing-muxp-%E2%80%93-mesh-updater-x-plane/) (Mesh Updater X-Plane) tool by @schmax.

Its purpose is helping users creating .muxp files.
Users will be able to work with WED and visualize muxp elements inside it.  

Inside WED all muxp elements must resides within a group called "muxp"
muxp_conv uses existing WED elements:

Header muxp elements are translated as follows:
* 'muxp_version', 'version', 'elevation', 'width', 'profile_interval': objects
* 'area': draped polygons 

Each command ("cut_polygon", "cut_ramp", "cut_flat_terrain_in_mesh", etc.) is treated 
as a group containing more WED elements as follows:
* 'coordinates', 'road_coords_drapped': draped polygons
* '3d_coordinates': strings
* 'name', 'elevation', 'terrain', 'include_raster_square_criteria', 'edge_limit', 'width', 'profile_interval': objects

See included examples to check how they are implemented.
    
muxp_conv can be used from the command line and it offers 2 commands:

* muxp2wed: is used to read an existing .muxp file and injecting his content inside an existing WED file
* wed2muxp: is used to create a .muxp file starting from a WED file that contains the needed muxp elements

**muxp2wed**

The WED file must exists.
I suggest to create a dedicated scenery (eg "_MUXP_ICAOCode_MyAiport") and then import the gateway scenery 
(or a custom one if avaliable) and then remove all unnecessary elements but the runway.

muxp_conv will create a muxp group with all the elements contained inside the source .muxp file.

**wed2muxp2**

muxp_conv will read the saved earth.wed.xml specified as input file and generate a .muxp file accordingly. 


**Syntax usage:**  

`
muxp_conv [-h] [-v] [-n] [-y] command input_file target_file
`

muxp_conv will create a backup file everytime it is going to overwrite a target file.

Optional arguments can be specified in order to avoid these backups or to force the overwriting of the target file.   

**installation**

In order to run the script you should install the requirements specified inside the file requirements.txt
(always use a virtualenv if possible) 

`
pip install -r requirements.txt
`



**Syntax examples:**

* Injecting a MUXP file into a WED file:  
`
python muxp_conv.py -y muxp2wed c:\pysrc\muxp\muxpfiles\SantaRosaIsland.muxp "d:\X-Plane-11\Custom Scenery\_MUXP_CLX3_SantaRosaIslandAirstrip\earth.wed.xml"
`

* Creating a MUXP file from a WED file:  
`
python muxp_conv.py -y wed2muxp "d:\X-Plane-11\Custom Scenery\_MUXP_CLX3_SantaRosaIslandAirstrip\earth.wed.xml" c:\pysrc\muxp_files\+30-130\clx3.muxp`
