import argparse
from datetime import datetime
from pathlib import Path
from zipfile import ZipFile, ZIP_DEFLATED

import tests
from muxp_conv.muxp import MUXP


def init_argparse() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(
        prog='muxp_conv',
        description="A converter from WED to MUXP and backward",
        add_help=True,
    )
    # parser.add_argument(
    #     '-h',
    #     help="""
    #         MUXP Converter has two main functions:
    #         1) Converting appropriate X-Plane WED files to MUXP format
    #         2) Reading, parsing and injecting MUXP files into X-Plane WED files
    #
    #         Available commands:
    #         wed2muxp                Converts a WED file to a MUXP file
    #         muxp2wed                Injects a MUXP file into a WED file
    #
    #         Options:
    #         -h, --help              show this help message and exit
    #         -f, --force-overwrite   Do not ask user confirmation for overwriting the target file
    #         """
    #     )
    parser.add_argument(
        "command", type=str,
        help="available commands are: 'wed2muxp' and 'muxp2wed'"
    )
    parser.add_argument("-v", "--version", action="version", version=f"{parser.prog} version 1.0")
    parser.add_argument(
        "-n", "--no-backup", action="store_true", default=False,
        dest='no_backup',
        help="Do not create a backup of the existing target file",
    )
    parser.add_argument(
        "-y", "--yes", action="store_true", default=False,
        dest='do_not_ask_user_for_overwriting',
        help="Do not ask user confirmation for overwriting the target file"
    )
    parser.add_argument("input_file", type=str, help="Input file")
    parser.add_argument("target_file", type=str, help="Target file")
    return parser


def backup_target_file(target_file):

    tf = Path(target_file)
    backups_dir = tf.parent / "muxp_conv_backups"
    if not backups_dir.exists():
        backups_dir.mkdir()

    backup_filename = Path(target_file + "_" + datetime.now().strftime("%Y%m%d%H%M%S") + ".zip").name
    backup_file = backups_dir / backup_filename
    zf = ZipFile(backup_file, "w", ZIP_DEFLATED)
    zf.write(args.target_file)
    zf.close()

    print(f"Created backup file into {backup_file}")


if __name__ == '__main__':

    parser = init_argparse()
    args = parser.parse_args()

    if args.command not in ("wed2muxp", "muxp2wed"):
        print("Error: invalid command, available commands are: 'wed2muxp' and 'muxp2wed'")
        exit()

    if not Path(args.input_file).is_file():
        print(f"Error: input file {args.input_file} not found")
        exit()

    target_file_exists = Path(args.target_file).is_file()

    if args.command == "muxp2wed" and not target_file_exists:
        print(
            f"Error: WED target file {args.target_file} must exists.\n"
            f"*** Hint: create a WED file from scratch and import the airport from the gateway ***")
        exit()

    if not args.do_not_ask_user_for_overwriting and target_file_exists:
        proceed = input("Target file exists, do you want to proceed? (yes/no)")
        if not proceed.lower() in ('y', 'yes'):
            exit()

    if args.command == "wed2muxp":

        muxp = MUXP()
        muxp_yaml = muxp.wed2muxp(wed_file=args.input_file)
        if muxp_yaml:
            if not args.no_backup and target_file_exists:
                backup_target_file(args.target_file)
            with open(args.target_file, 'w') as f:
                f.write(muxp_yaml)
            print(f"Muxp file {args.target_file} generated")

    elif args.command == "muxp2wed":

        muxp = MUXP(muxp_file=args.input_file)
        wed_xml = muxp.wed_inject(wed_file=args.target_file)
        if wed_xml:
            if not args.no_backup and target_file_exists:
                backup_target_file(args.target_file)
            with open(args.target_file, 'w') as f:
                f.write(wed_xml)
            print("Muxp content injected! Revert to saved file if WorldEditor already opened.")

    # tests.create_replace_muxp_group()
    # tests.yaml_test()
    # tests.yaml2wed_test_lflj()
    # tests.wed2yaml_test_lflj()
    # tests.wed2muxp(
    #     wed_file="c:\\X-Plane-11\\Custom Scenery\\_FR-LFLJ_Courchevel\\earth.wed.xml",
    #     muxp_file="c:\\pysrc\\muxp_files\\+45+006\\LFLJ_wed_extract.muxp",
    # )

    # tests.yaml2wed(
    #     muxp_file="c:\\pysrc\\muxp\\muxpfiles\\Tunnel_Ramp_KMSP.muxp",
    #     wed_file="c:\\X-Plane-11\\Custom Scenery\\_MUXP_KMSP\\earth.wed.xml"
    # )

    # tests.wed2muxp(
    #     wed_file="c:\\X-Plane-11\\Custom Scenery\\_MUXP_FR-LFLJ_Courchevel\\earth.wed.xml",
    #     muxp_file="c:\\pysrc\\muxp_files\\+45+006\\LFLJ_wed.muxp",
    # )

    # tests.wed2muxp(
    #     wed_file="c:\\X-Plane-11\\Custom Scenery\\_MUXP_PAKY_Karluk\\earth.wed.xml",
    #     muxp_file="c:\\pysrc\\muxp_files\\+57-155\\PAKY_Karluk_wed.muxp",
    # )

