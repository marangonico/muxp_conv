from muxp_conv.muxp import MUXP
from muxp_conv.wed import WED
from yaml import dump


def create_replace_muxp_group():

    wed = WED(wed_file="d:\\X-Plane-11\\Custom Scenery\\test_muxp\\earth.wed.xml")

    group_muxp = wed.get_object('Group', 'muxp')
    if group_muxp:
        wed.delete_object(group_muxp)

    group = wed.add_object('Group', 'muxp', wed.world)
    polygon = wed.add_object('PolygonPlacement', 'muxp_poly', group)
    ring = wed.add_object('Ring', 'muxp_ring', polygon)
    wed.add_simple_bezier_boundary_node(object_name='muxp_node', parent=ring, lat=45.408282, lon=6.624953)
    wed.add_simple_bezier_boundary_node(object_name='muxp_node', parent=ring, lat=45.397456, lon=6.650509)
    wed.add_simple_bezier_boundary_node(object_name='muxp_node', parent=ring, lat=45.390056, lon=6.632071)

    group_muxp_001 = wed.get_object('Group', 'muxp_001')
    if group_muxp_001:
        wed.delete_object(group_muxp_001)

    xml = wed.emit_xml()


def yaml_test():

    muxp = MUXP("c:\\pysrc\\muxp\\muxpfiles\\LFLJ.muxp")
    yaml = dump(muxp.muxp_yaml, default_flow_style=False, sort_keys=False)
    pass


def yaml2wed(muxp_file, wed_file):

    muxp = MUXP(muxp_file=muxp_file)
    xml = muxp.wed_inject(wed_file=wed_file)

    with open(wed_file, 'w') as f:
        f.write(xml)

    pass


def yaml2wed_test_lflj():

    muxp = MUXP("c:\\pysrc\\muxp_files\\+45+006\\LFLJ_import.muxp")
    xml = muxp.wed_inject(wed_file="c:\\X-Plane-11\\Custom Scenery\\_FR-LFLJ_Courchevel\\earth.wed.xml")

    pass


def wed2yaml_test_lflj():

    muxp = MUXP()
    # yaml = muxp.wed2muxp(wed_file="d:\\X-Plane-11\\Custom Scenery\\test_muxp\\earth.wed.xml")
    muxp_file = "c:\\pysrc\\muxp_files\\+45+006\\LFLJ_wed.muxp"
    wed_file = "c:\\X-Plane-11\\Custom Scenery\\_FR-LFLJ_Courchevel\\earth.wed.xml"

    yaml = muxp.wed2muxp(wed_file="c:\\X-Plane-11\\Custom Scenery\\_FR-LFLJ_Courchevel\\earth.wed.xml")
    with open(muxp_file, 'w') as f:
        f.write(yaml)

    pass


def wed2muxp(wed_file, muxp_file):

    muxp = MUXP()
    # yaml = muxp.wed2muxp(wed_file="d:\\X-Plane-11\\Custom Scenery\\test_muxp\\earth.wed.xml")
    yaml = muxp.wed2muxp(wed_file=wed_file)
    with open(muxp_file, 'w') as f:
        f.write(yaml)

    pass
